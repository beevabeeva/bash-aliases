# Lenovo aliases

alias jupyter=/opt/anaconda3/bin/jupyter-notebook
alias tupsy='echo hello tupsy'
alias teepa='echo teepa tup'
alias ebash="subl ~/.bashrc"
alias src="source ~/.bashrc; echo sourced bashrc"
alias sshms="ssh abank@146.141.21.100"

# ms cluster aliases:

alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*$

alias tupsy='echo hello tupsy'

alias teepa='echo teepa tups you'

alias teper=teepa

alias s="cd ~/slurms/"

alias cgdb="/usr/local/cuda-10.0/bin/cuda-gdb"

alias sq="squeue"

alias src="source ~/.bashrc; echo sourced bash"

alias ebash="nano ~/.bashrc"

alias dquery="/usr/local/cuda-10.0/extras/demo_suite/deviceQuery"

alias gcm="git commit -a -m"

alias git-env="git config --global credential.helper 'cache --timeout=3600'"

alias tups="mscluster23,mscluster24,mscluster25,mscluster26,mscluster27,mscluster28,mscluster30,mscluster31,mscluster33,mscluster34,mscluster35,msclu$

alias ms='f(){ ssh mscluster"$1";  unset -f f; }; f'

alias xpanes="/home-mscluster/abank/scripts/xpanes"

alias monitor="python /home-mscluster/abank/scripts/multi_tmux.py"

alias nvtop="/home-mscluster/abank/scripts/nvtop/bin/nvtop"

alias en="source atm-venv/bin/activate"

alias thunder="virtualenv -p $(which python3.6) thunder; source thunder/bin/activate"

alias die="deactivate"

alias clc="clear"

alias atmtest="atm enter_data --train-path /home-mscluster/abank/research/ATM/atm/demos/pollution.csv"

alias atm-credit="atm enter_data --train-path /home-mscluster/abank/research/ATM/atm/demos/credit-g_1.csv"

alias work="cd /home-mscluster/abank/research/ATM/;en"

alias worker="atm worker"


